package com.inter.challenge.exception;

import com.inter.challenge.utils.exception.ResponseException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

@RunWith(MockitoJUnitRunner.class)
public class ResponseExceptionTest {

    @Test(expected = ResponseException.class)
    public void ResponseExceptionWithMessage() {
        throw new ResponseException("Test Exception");
    }

    @Test(expected = ResponseException.class)
    public void ResponseExceptionWithMessageAndStatus() {
        throw new ResponseException("Test Exception", HttpStatus.BAD_GATEWAY);
    }

    @Test(expected = ResponseException.class)
    public void ResponseExceptionWithMessageThrowableAndStatus() {
        throw new ResponseException("Test Exception", new Throwable(), HttpStatus.BAD_GATEWAY);
    }
}