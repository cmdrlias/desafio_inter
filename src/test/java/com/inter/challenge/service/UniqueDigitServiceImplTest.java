package com.inter.challenge.service;

import com.inter.challenge.model.UniqueDigit;
import com.inter.challenge.repository.UniqueDigitRepository;
import com.inter.challenge.service.impl.UniqueDigitServiceImpl;
import com.inter.challenge.utils.exception.ResponseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UniqueDigitServiceImplTest {
    @Mock
    UniqueDigitRepository uniqueDigitRepository;

    @InjectMocks
    @Spy
    UniqueDigitServiceImpl uniqueDigitService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findAll() {
        List<UniqueDigit> response = new ArrayList<>();
        Mockito.when(uniqueDigitRepository.findAll()).thenReturn(response);

        uniqueDigitService.findAll();
    }

    @Test(expected = ResponseException.class)
    public void findAllException() {
        Mockito.when(uniqueDigitRepository.findAll()).thenThrow(new ResponseException("Exception for findAll"));

        uniqueDigitService.findAll();
    }

    @Test
    public void createTest() {
        UniqueDigit digit = newUniqueDigit();

        Mockito.when(uniqueDigitRepository.save(digit)).thenReturn(digit);

        uniqueDigitService.create(digit);

        Mockito.verify(uniqueDigitService, Mockito.atMostOnce()).create(digit);
    }

    @Test(expected = ResponseException.class)
    public void createNull() {
        uniqueDigitService.create(null);

        Mockito.verify(uniqueDigitService, Mockito.atMostOnce()).create(null);
    }

    @Test
    public void getUniqueDigit() {
        UniqueDigit digit = newUniqueDigit();
        uniqueDigitService.getUniqueDigit(digit.getN(), digit.getK());

        assert uniqueDigitService.getUniqueDigit(digit.getN(), digit.getK()) == 6;

        Mockito.verify(uniqueDigitService, Mockito.atLeastOnce()).getUniqueDigit(digit.getN(), digit.getK());
    }

    public UniqueDigit newUniqueDigit() {
        return new UniqueDigit((long) 1, "123", 4);
    }
}
