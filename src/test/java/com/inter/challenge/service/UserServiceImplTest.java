package com.inter.challenge.service;

import com.inter.challenge.model.User;
import com.inter.challenge.repository.UserRepository;
import com.inter.challenge.service.impl.UserServiceImpl;
import com.inter.challenge.utils.exception.ResponseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {
    @Mock
    UserRepository userRepository;

    @InjectMocks
    @Spy
    UserServiceImpl userService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findAll() {
        List<User> response = new ArrayList<>();
        Mockito.when(userRepository.findAll()).thenReturn(response);

        userService.findAll();
    }

    @Test(expected = ResponseException.class)
    public void findAllException() {
        Mockito.when(userRepository.findAll()).thenThrow(new ResponseException("Exception for findAll"));

        userService.findAll();
    }

    @Test
    public void findById() {
        Optional<User> response = Optional.of(new User());
        Mockito.when(userRepository.findById((long) 1)).thenReturn(response);

        userService.findById((long) 1);
    }

    @Test(expected = ResponseException.class)
    public void findByIdNull() {
        Mockito.when(userRepository.findById(null)).thenThrow(new ResponseException("Exception for findById"));

        userService.findById(null);
    }

    @Test
    public void create() {
        User user = newUser();

        Mockito.when(userRepository.save(user)).thenReturn(user);

        userService.create(user);

        Mockito.verify(userService, Mockito.atMostOnce()).create(user);
    }

    @Test(expected = ResponseException.class)
    public void createNull() {

        userService.create(null);

        Mockito.verify(userService, Mockito.atMostOnce()).create(null);
    }

    @Test
    public void update() {
        User user = newUser();

        Mockito.when(userRepository.save(user)).thenReturn(user);

        userService.update(user);

        Mockito.verify(userService, Mockito.atMostOnce()).update(user);
    }

    @Test(expected = ResponseException.class)
    public void updateNull() {
        userService.update(null);

        Mockito.verify(userService, Mockito.atMostOnce()).update(null);
    }

    @Test
    public void delete() {
        Optional<User> response = Optional.of(new User());
        Mockito.when(userRepository.findById((long) 1)).thenReturn(response);
        Mockito.doNothing().when(userRepository).delete(response.get());

        userService.delete((long) 1);

        Mockito.verify(userService, Mockito.atMostOnce()).delete((long) 1);
    }

    @Test(expected = ResponseException.class)
    public void deleteThrowsException() {
        Optional<User> response = Optional.of(new User());

        Mockito.when(userRepository.findById((long) 0)).thenThrow(new ResponseException("Delete exception"));

        userService.delete((long) 0);

        Mockito.verify(userService, Mockito.atMostOnce()).delete((long) 0);
    }

    public User newUser() {
        return new User((long) 1, "name", "email", "password");
    }
}
