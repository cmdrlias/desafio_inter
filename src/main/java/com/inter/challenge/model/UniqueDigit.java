package com.inter.challenge.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class UniqueDigit {
    @Id
    @GeneratedValue
    private Long id;

    private String n;
    private Integer k;
    private Integer r;

    @ManyToOne
    @JoinColumn(name = "user_id", updatable = true, insertable = true)
    @JsonBackReference
    private User user;

    public UniqueDigit(Long id, String n, Integer k) {
        this.id = id;
        this.n = n;
        this.k = k;
    }

    public UniqueDigit(Long id, String n, Integer k, Integer r, User user) {
        this.id = id;
        this.n = n;
        this.k = k;
        this.r = r;
        this.user = user;
    }

    public UniqueDigit() {
        this.user = new User();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }

    public Integer getK() {
        return k;
    }

    public void setK(Integer k) {
        this.k = k;
    }

    public Integer getR() {
        return r;
    }

    public void setR(Integer r) {
        this.r = r;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
