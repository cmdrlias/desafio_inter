package com.inter.challenge.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String email;
    private String password;

    @OneToMany
    @JoinColumn(name="user_id", nullable=true)
    @JsonManagedReference
    List<UniqueDigit> uniqueDigitList;

    public User() {
        this.uniqueDigitList = new ArrayList<>();
    }

    public User(Long id, String name, String email, String password, List<UniqueDigit> uniqueDigitList) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.uniqueDigitList = uniqueDigitList;
    }

    public User(Long id, String name, String email, String password) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = new BCryptPasswordEncoder().encode(password);
    }

    public List<UniqueDigit> getUniqueDigitList() {
        return uniqueDigitList;
    }

    public void setUniqueDigitList(List<UniqueDigit> uniqueDigitList) {
        this.uniqueDigitList = uniqueDigitList;
    }

    public void addToList(UniqueDigit uniqueDigit) {
        this.uniqueDigitList.add(uniqueDigit);
    }
}
