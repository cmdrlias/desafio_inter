package com.inter.challenge.repository;

import com.inter.challenge.model.UniqueDigit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UniqueDigitRepository extends CrudRepository<UniqueDigit, Long> {
}
