package com.inter.challenge.controller;

import com.inter.challenge.model.UniqueDigit;
import com.inter.challenge.model.User;
import com.inter.challenge.service.UniqueDigitService;
import com.inter.challenge.service.UserService;
import com.inter.challenge.utils.exception.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(value = {"/all"}, method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public List<User> getAll() {
        try {
            return userService.findAll();
        } catch (ResponseException ex) {
            log.error("Error while getting user list = {}", ex.getStatus());
            throw ex;
        }
    }

    @RequestMapping(value = {"/get/{id}"}, method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public Optional<User> getById(@PathVariable("id") Long id) {
        return userService.findById(id);
    }

    @RequestMapping(value = {"/new"}, method = RequestMethod.POST, consumes="application/json")
    @ResponseBody
    public ResponseEntity<?> create(@RequestBody User user) {
        try {
            userService.create(user);
            log.info("New user {} created successfully",
                    user.getEmail());

            return new ResponseEntity<>("User created successfully!", HttpStatus.CREATED);
        } catch(ResponseException ex) {
            return new ResponseEntity<>( String.format("Error while trying to create user, %s", ex.getStatus()), HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = {"/edit/{id}"}, method = RequestMethod.POST, consumes="application/json")
    @ResponseBody
    public ResponseEntity<?> update(@RequestBody User user, @PathVariable("id") Long id) {
        try {
            user.setId(id);
            userService.update(user);
            log.info("User {} was updated.", user.getId());

            return new ResponseEntity<>("User updated successfully!", HttpStatus.OK);
        } catch(ResponseException ex) {
            return new ResponseEntity<>( String.format("Error while trying to update user with id = %s, %s", id, ex.getStatus()), HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = {"/delete/{id}"}, method = RequestMethod.DELETE)
    @ResponseBody
    public String delete(@PathVariable("id") Long id) {
        try {
            userService.delete(id);
            return String.format("User %s deleted successfully.", id);
        } catch(ResponseException ex) {
            return String.format("Error while trying to delete user with id = %s, %s", id, ex.getStatus());
        }
    }
}