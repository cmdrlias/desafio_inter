package com.inter.challenge.controller;

import com.inter.challenge.model.UniqueDigit;
import com.inter.challenge.model.User;
import com.inter.challenge.service.UniqueDigitService;
import com.inter.challenge.service.UserService;
import com.inter.challenge.utils.exception.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/unique-digit")
public class UniqueDigitController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UniqueDigitService uniqueDigitService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = {"/new/{user-id}"}, method = RequestMethod.POST, consumes="application/json")
    @ResponseBody
    public ResponseEntity<?> create(@RequestBody UniqueDigit uniqueDigit, @PathVariable("user-id") Long id) {
        uniqueDigit.setR(uniqueDigitService.getUniqueDigit(uniqueDigit.getN(), uniqueDigit.getK()));

        if(id != 0) {
            Optional<User> user = userService.findById(id);

            if(user.isPresent()) {
                user.get().addToList(uniqueDigit);
                uniqueDigit.setUser(user.get());
            }
        }

        uniqueDigitService.create(uniqueDigit);

        log.info("New unique digit for {} calculated successfully",
                uniqueDigit.getK());

        return new ResponseEntity<>("New unique digit calculated successfully!", HttpStatus.CREATED);
    }

    @RequestMapping(value = {"/all"}, method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public List<UniqueDigit> getAll() {
        try {
            return uniqueDigitService.findAll();
        } catch (ResponseException ex) {
            log.error("Error while getting unique digit list = {}", ex.getStatus());
            throw ex;
        }
    }
}
