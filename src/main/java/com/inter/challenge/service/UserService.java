package com.inter.challenge.service;

import com.inter.challenge.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> findAll();
    Optional<User> findById(Long id);
    void create(User user);
    void update(User user);
    void delete(Long id);
}
