package com.inter.challenge.service;

import com.inter.challenge.model.UniqueDigit;
import com.inter.challenge.model.User;

import java.util.List;

public interface UniqueDigitService {
    List<UniqueDigit> findAll();

    Integer getUniqueDigit(String n, int k);

    void create(UniqueDigit uniqueDigit);
}
