package com.inter.challenge.service.impl;

import com.inter.challenge.model.User;
import com.inter.challenge.repository.UserRepository;
import com.inter.challenge.service.UserService;
import com.inter.challenge.utils.exception.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> findAll() {
        try {
            return (List<User>) userRepository.findAll();
        } catch (ResponseException ex) {
            log.error("Error while finding user list = {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public Optional<User> findById(Long id) {
        try {
            return userRepository.findById(id);
        } catch(ResponseException ex) {
            log.error("Error while finding user with id = {}, {}", id, ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void create(User user) {
        try {
            if (user != null) {
                if(user.getUniqueDigitList() == null)
                    user.setUniqueDigitList(new ArrayList<>());

                userRepository.save(user);
            } else {
                log.warn("user can't be null");
                throw new ResponseException("User can't be null", HttpStatus.CONFLICT);
            }
        } catch(ResponseException ex) {
            log.error("Error while creating user, {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void update(User user) {
        try {
            if (user != null) {
                userRepository.save(user);
            } else {
                log.warn("User can't be null");
                throw new ResponseException("User can't be null", HttpStatus.BAD_REQUEST);
            }
        } catch(ResponseException ex) {
            log.error("Error while updating user, {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void delete(Long id) {
        try {
            Optional<User> user = userRepository.findById(id);
            user.ifPresent(u -> userRepository.delete(u));
        } catch(ResponseException ex) {
            log.error("Error while deleting user {}, {}", id, ex.getStatus());
            throw ex;
        }
    }
}
