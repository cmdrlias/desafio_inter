package com.inter.challenge.service.impl;

import com.inter.challenge.model.UniqueDigit;
import com.inter.challenge.repository.UniqueDigitRepository;
import com.inter.challenge.service.UniqueDigitService;
import com.inter.challenge.utils.exception.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UniqueDigitServiceImpl implements UniqueDigitService {

    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UniqueDigitRepository uniqueDigitRepository;

    @Override
    public List<UniqueDigit> findAll() {
        try {
            return (List<UniqueDigit>) uniqueDigitRepository.findAll();
        } catch (ResponseException ex) {
            log.error("Error while finding unique digit list = {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void create(UniqueDigit uniqueDigit) {
        try {
            if (uniqueDigit != null) {
                uniqueDigitRepository.save(uniqueDigit);
            } else {
                log.warn("uniqueDigit can't be null");
                throw new ResponseException("uniqueDigit can't be null", HttpStatus.CONFLICT);
            }
        } catch(ResponseException ex) {
            log.error("Error while saving unique digit, {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public Integer getUniqueDigit(String n, int k) {
        String digit = n;

        for (int i = 0; i < k - 1; i++)
            digit += n;

        while (digit.length() > 1)
            digit = isUnique(digit);

        return Integer.parseInt(digit);
    }

    private String isUnique(String digit) {
        int number = 0;

        for(int i = 0; i < digit.length(); i ++)
            number += Integer.parseInt(""+ digit.charAt(i));

        return "" + number;
    }
}
